document.addEventListener('scroll', () => {
  let header = document.querySelector('#itemHeader')
  if (window.scrollY > 30) {
    header.classList.add('hidden')
  } else {
    header.classList.remove('hidden')
  }
})